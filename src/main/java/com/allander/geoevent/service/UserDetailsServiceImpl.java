package com.allander.geoevent.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.allander.geoevent.model.Account;
import com.allander.geoevent.repository.AccountRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    AccountRepository accountRepository;
	
    Logger logger = Logger.getLogger(UserDetailsServiceImpl.class);
    
	@Override
	public UserDetails loadUserByUsername(String username) {
		Account account = accountRepository.findByUserName(username);

		if(account!=null) {
			logger.info("Logging in as user: " + username + "(DB:" + account.getUserName() + ")");
	        return new UserDetailsImpl(account);
		} else {
			logger.info("Failed to login as user: " + username);
			 throw new UsernameNotFoundException(username);
		}
        
		
//		UserBuilder builder = null;
//		if(account!=null) {
//			User user = new User(account.getUserName(), account.getPassword(), true, true, true, true,
//	                AuthorityUtils.createAuthorityList("USER"));
//			logger.info("Loggat in med: " + user.getUsername() + " pass: " + user.getPassword());
//			builder = org.springframework.security.core.userdetails.User.withUsername(username);
//		    builder.password(new BCryptPasswordEncoder().encode(user.getPassword()));
//		    builder.roles("USER");
//			logger.info("Pass encrypt: " + builder.build().getPassword());
//		} else {
//			logger.info("Cant find user " + username);
//			 throw new UsernameNotFoundException("User not found.");
//
//		}
		
//    	return builder.build();
	}
}

