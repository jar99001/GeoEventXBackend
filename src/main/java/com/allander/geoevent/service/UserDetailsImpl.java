package com.allander.geoevent.service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.allander.geoevent.model.Account;

public class UserDetailsImpl implements UserDetails {
	
    Logger logger = Logger.getLogger(UserDetailsImpl.class);
	
	private static final long serialVersionUID = 1L;
	private Account account;

	public UserDetailsImpl(Account account) {
		this.account = account;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		logger.info("Getting Authorities");
        Set<GrantedAuthority> grantedAuth = new HashSet<GrantedAuthority>();

        if(account!=null) {
            	grantedAuth.add(new SimpleGrantedAuthority(account.getRole()));
        } else {
        	return null;
        }
		return grantedAuth;
	}

	@Override
	public String getPassword() {
		logger.info("Getting password");
		return account.getPassword();
	}

	@Override
	public String getUsername() {
		logger.info("Getting username");
		return account.getUserName();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}
