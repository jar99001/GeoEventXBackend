package com.allander.geoevent.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, 
        allowGetters = true)
public class Comment {
	@Id
	@GeneratedValue
	private Long id;

	@Column(length=50)
	private String userName;
	
	@Column(length=5000)
	private String comment;
	
	@ManyToOne//(cascade = {CascadeType.ALL})
	@JsonBackReference
	private GeoEvent geoEvent;

    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date time;

    public Comment() {
    	
    }
    
    public Comment(String userName, String comment, GeoEvent geoEvent) {
    	this.userName = userName;
    	this.comment = comment;
    	this.geoEvent = geoEvent;
    }
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public GeoEvent getGeoEvent() {
		return geoEvent;
	}

	public void setGeoEventId(GeoEvent geoEvent) {
		this.geoEvent = geoEvent;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	@Override
	public String toString() {
		return "Comment [id=" + id + ", userName=" + userName + ", comment=" + comment + ", time=" + time + "]";
	}
	
	
}