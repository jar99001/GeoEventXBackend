package com.allander.geoevent.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, 
        allowGetters = true)
public class Account {

	@Id
	@GeneratedValue
	private Long id;

	@Column(length=50,nullable = false, unique = true)
	private String userName;

	@Column(length=250)
	private String email;

	@Column(length=255)
	@JsonIgnore
	private String password;

	@Column(length=50)
	private String role;
	
	@OneToMany(mappedBy="account",cascade=CascadeType.ALL,fetch=FetchType.LAZY)
    @JsonManagedReference
	@JsonIgnore
	private List<GeoStamp> geoStamps;

	@OneToMany(mappedBy="ownerAccount",cascade=CascadeType.ALL,fetch=FetchType.LAZY)
    @JsonManagedReference
	@JsonIgnore
	private List<GeoEvent> geoEvents;

	@ManyToMany(cascade = { 
	        CascadeType.PERSIST, 
	        CascadeType.MERGE
	    })
	    @JoinTable(name = "account_guestGeoEvent",
	        joinColumns = @JoinColumn(name = "account_id"),
	        inverseJoinColumns = @JoinColumn(name = "guestGeoEvent_id")
	    )
	private Set<GeoEvent> guestGeoEvent= new HashSet<>();

	public Account() {}
	
	public Account(String userName, String email, String password, String role) {
		this.userName = userName;
		this.email = email;
		this.password = password;
		this.role = role;
	}


	// Getters & Setters

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	
	
	public List<GeoStamp> getGeoStamps() {
		return geoStamps;
	}

	public void setGeoStamps(List<GeoStamp> geoStamps) {
		this.geoStamps = geoStamps;
	}

	public List<GeoEvent> getGeoEvents() {
		return geoEvents;
	}

	public void setGeoEvents(List<GeoEvent> geoEvents) {
		this.geoEvents = geoEvents;
	}

	public Set<GeoEvent> getGuestGeoEvent() {
		return guestGeoEvent;
	}

	public void setGuestGeoEvent(Set<GeoEvent> guestGeoEvent) {
		this.guestGeoEvent = guestGeoEvent;
	}

	@Override
	public String toString() {
		
		return this.userName + ", " + this.email + ", " + this.password + ", " + this.role;
	}
	
}