package com.allander.geoevent.model;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, 
        allowGetters = true)
public class GeoEvent {
	@Id
	@GeneratedValue
	private Long id;


	@Column(length=50)
	private String eventName;
	
	@Column(length=5000)
	private String description;

	// GPS-Coordinates
	private double longitude;

	private double latitude;

	private int radius;
	
    @Temporal(TemporalType.TIMESTAMP)
    private Date time;
    
    private int timeSpanBefore;

    private int timeSpanAfter;

    private Long originalGeoStampId;
    
	@OneToMany(mappedBy="geoEvent",cascade=CascadeType.ALL,fetch=FetchType.EAGER)
    @JsonManagedReference
	private List<Comment> comments;

	@ManyToOne
	@JsonBackReference
	private Account ownerAccount;

	@JsonIgnore
	@ManyToMany(mappedBy = "guestGeoEvent")
    private Set<Account> guestAccounts = new HashSet<>();

	private boolean selfCreated;


	public boolean isSelfCreated() {
		return selfCreated;
	}

	public void setSelfCreated(boolean selfCreated) {
		this.selfCreated = selfCreated;
	}

	public Set<Account> getGuestAccounts() {
		return guestAccounts;
	}

	public void setGuestAccounts(Set<Account> guestAccounts) {
		this.guestAccounts = guestAccounts;
	}

	public Account getOwnerAccount() {
		return ownerAccount;
	}

	public void setOwnerAccount(Account ownerAccount) {
		this.ownerAccount = ownerAccount;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public int getTimeSpanBefore() {
		return timeSpanBefore;
	}

	public void setTimeSpanBefore(int timeSpanBefore) {
		this.timeSpanBefore = timeSpanBefore;
	}

	public int getTimeSpanAfter() {
		return timeSpanAfter;
	}

	public void setTimeSpanAfter(int timeSpanAfter) {
		this.timeSpanAfter = timeSpanAfter;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public Long getOriginalGeoStampId() {
		return originalGeoStampId;
	}

	public void setOriginalGeoStampId(Long originalGeoStampId) {
		this.originalGeoStampId = originalGeoStampId;
	}

	@Override
	public String toString() {
		return "GeoEvent [id=" + id + ", eventName=" + eventName + ", description=" + description + ", longitude="
				+ longitude + ", latitude=" + latitude + ", radius=" + radius + ", time=" + time + ", timeSpanBefore="
				+ timeSpanBefore + ", timeSpanAfter=" + timeSpanAfter + ", originalGeoStampId=" + originalGeoStampId
				+ ", comments=" + comments + ", ownerAccount=" + ownerAccount + ", guestAccounts=" + guestAccounts
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GeoEvent other = (GeoEvent) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	
	
}