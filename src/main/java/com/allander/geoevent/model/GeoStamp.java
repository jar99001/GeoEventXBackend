package com.allander.geoevent.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, 
        allowGetters = true)
public class GeoStamp {

	@Id
	@GeneratedValue
	private Long id;

	// GPS-Coordinates
	private double longitude;

	private double latitude;

	private Integer accuracy;
	
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date time;
	
	@ManyToOne//(cascade = {CascadeType.ALL})
	@JsonBackReference
	private Account account;

	public GeoStamp() {}
	
	public GeoStamp(double longitude, double latitude, Account account) {
		this.longitude = longitude;
		this.latitude = latitude;
		this.account = account;
	}

	// Getters & Setters
	
	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Long getId() {
		return id;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public Date getTime() {
		return time;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Integer getAccuracy() {
		return accuracy;
	}

	public void setAccuracy(Integer accuracy) {
		this.accuracy = accuracy;
	}
	
	
}