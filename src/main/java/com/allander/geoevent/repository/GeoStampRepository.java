package com.allander.geoevent.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.allander.geoevent.model.Account;
import com.allander.geoevent.model.GeoStamp;

@Repository
public interface GeoStampRepository extends JpaRepository<GeoStamp, Long> {
	public List<GeoStamp> findByAccount(Account account);
}