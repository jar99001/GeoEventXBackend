package com.allander.geoevent.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.allander.geoevent.model.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

	public Account findByUserName(String userName);
}
