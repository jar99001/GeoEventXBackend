package com.allander.geoevent.repository;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.allander.geoevent.model.Account;
import com.allander.geoevent.model.GeoEvent;

@Repository
public interface GeoEventRepository extends JpaRepository<GeoEvent, Long> {
//	@Query("SELECT u FROM GeoEvent u WHERE u.time between '2018-08-06 14:46:19' and '2019-01-01'")
	@Query("SELECT u FROM GeoEvent u WHERE (:tid between SUBTIME(u.time,'0:30') AND ADDTIME(u.time,'0:30'))"
			+ " AND (:mylon BETWEEN (u.longitude-:lon) AND (u.longitude+:lon))"
			+ " AND (:mylat BETWEEN (u.latitude-:lat) AND (u.latitude+:lat))")
	List<GeoEvent> findAllGeoEvents(
			@Param("tid") Date tid,
			@Param("mylon") double mylon,
			@Param("mylat") double mylat,
			@Param("lon") double lon,
			@Param("lat") double lat);
	
	public List<GeoEvent> findByOwnerAccount(Account account);
}