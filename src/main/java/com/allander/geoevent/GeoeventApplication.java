package com.allander.geoevent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.allander.geoevent.model.Account;
import com.allander.geoevent.repository.AccountRepository;
import com.allander.geoevent.service.UserDetailsServiceImpl;

@SpringBootApplication
@EnableJpaAuditing
public class GeoeventApplication {

	
	public static void main(String[] args) {
		SpringApplication.run(GeoeventApplication.class, args);
	}
}

/*	

@Configuration
class WebSecurityConfiguration extends GlobalAuthenticationConfigurerAdapter {

	Logger logger = Logger.getLogger(WebSecurityConfiguration.class);


	  @Autowired
	  AccountRepository accountRepository;
	
	  @Override
	  public void init(AuthenticationManagerBuilder auth) throws Exception {
		  String password = passwordEncoder().encode("password");
//		  accountRepository.save(new Account("admin","admin@geoevent.com",password,"ADMIN"));

		  //password = passwordEncoder().encode("r29osary");
		  //accountRepository.save(new Account("johan","fam.allander@gmail.com",password,"USER"));
		  auth.userDetailsService(userDetailsService());
	  }

	  @Bean
	  public PasswordEncoder passwordEncoder() {
		  return new BCryptPasswordEncoder();
	  }
	
	  @Bean
	  UserDetailsService userDetailsService() {
	    return new UserDetailsService() {
	
	      @Override
	      public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
	    	  logger.info("UserDetails()");
	    	  Account account = accountRepository.findByUserName(username);

	        if(account != null) {
	        	logger.info("Now logging in as " + account);
	        	return MyUserPrincipal();
	        	return new User(account.getUserName(), account.getPassword(), true, true, true, true,
	                AuthorityUtils.createAuthorityList("ROLE_" + account.getRole()));
	        } else {
	          throw new UsernameNotFoundException(username);
	        }
	      }
	      
	    };
	  }
}
	  */


@Configuration
@EnableWebSecurity
class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	/*
    @Autowired
    private MyBasicAuthenticationEntryPoint authenticationEntryPoint;
 
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
          .withUser("johan").password("r29osary")
          .authorities("ROLE_USER");
    }
	*/

@Autowired
private UserDetailsServiceImpl userDetailsService;

@Autowired
AccountRepository accountRepository;

@Override
protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//	  accountRepository.save(new Account("Ala","ala.rebwar@gmail.com",encoder().encode("password"),"USER"));
//	  accountRepository.save(new Account("johan","fam.allander@gmail.com",encoder().encode("r29osary"),"USER"));

	  auth.authenticationProvider(authenticationProvider());
}

@Bean
public DaoAuthenticationProvider authenticationProvider() {
    DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
    authProvider.setUserDetailsService(userDetailsService);
    authProvider.setPasswordEncoder(encoder());
    return authProvider;
}
 
@Bean
public PasswordEncoder encoder() {
    return new BCryptPasswordEncoder(11);
}
	
@Override
protected void configure(HttpSecurity http) throws Exception {

	http
	.authorizeRequests()
	.antMatchers("/api/accounts/new").permitAll()
	.antMatchers("/api/accounts/my").authenticated()
	.antMatchers("/api/accounts/**").hasRole("ADMIN")

	.antMatchers("/api/geostamps").hasRole("ADMIN")
	.antMatchers("/api/geostamps/my").authenticated()
	.antMatchers("/api/geostamps/**").hasRole("ADMIN")
	
	.antMatchers("/api/geoevents").hasRole("ADMIN")
	.antMatchers("/api/geoevents/my").authenticated()
	.antMatchers("/api/geoevents/**").hasRole("ADMIN")
	.antMatchers("/res/csrf").permitAll()
	.anyRequest().authenticated()
    .and()
    .formLogin().permitAll()
    	.and()
	.logout()
		.logoutUrl("/logout")
		.permitAll().and()
	.httpBasic()
		.and()
		.csrf().disable();
}

//	@Bean
//	CorsConfigurationSource corsConfigurationSource() {
//		CorsConfiguration configuration = new CorsConfiguration();
//		configuration.setAllowedOrigins(Arrays.asList("http://localhost:3000"));
//		configuration.setAllowedMethods(Arrays.asList("GET","POST"));
//		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
//		source.registerCorsConfiguration("/**", configuration);
//		return source;
//	}
}
    
    
