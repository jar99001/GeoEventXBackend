package com.allander.geoevent.controller;

import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.allander.geoevent.exception.ResourceNotFoundException;
import com.allander.geoevent.model.Account;
import com.allander.geoevent.model.GeoEvent;
import com.allander.geoevent.model.GeoStamp;
import com.allander.geoevent.repository.AccountRepository;
import com.allander.geoevent.repository.GeoEventRepository;
import com.allander.geoevent.repository.GeoStampRepository;

@RestController
@RequestMapping("/api/geoevents")
public class GeoEventController {

	Logger logger = Logger.getLogger(GeoEventController.class);
	
    @Autowired
    GeoStampRepository geoStampRepository;

	@Autowired
    GeoEventRepository geoEventRepository;

    @Autowired
    AccountRepository accountRepository;
    
    @GetMapping("/my")
    public List<GeoEvent> getMyGeoEvents(Principal principal) {
    	Account account = accountRepository.findByUserName(principal.getName());
    	logger.info("Getting geoevents for user " + principal.getName() + "\n\t More info: " + account);
    	List<GeoEvent> geoEventList = geoEventRepository.findByOwnerAccount(account);
    	geoEventList.forEach(g->{
    		g.setSelfCreated(true);
    	});
    	Set<GeoEvent> guestList = account.getGuestGeoEvent();
    	guestList.forEach(g->{
    		g.setSelfCreated(false);
    	});
    	geoEventList.addAll(guestList);
    	return geoEventList;
    }

    
    
    @PostMapping("/my")
    public GeoEvent postMyGeoEvents(@Valid @RequestBody GeoEvent geoEvent, Principal principal) {
    	Account account = accountRepository.findByUserName(principal.getName());
    	Optional<GeoStamp> geoStamp = geoStampRepository.findById(geoEvent.getOriginalGeoStampId());
    	if(geoStamp.isPresent()) {
    		if(account.getId() == geoStamp.get().getAccount().getId()) {
    			logger.info("The account owns the geostamp.");
    			geoEvent.setTime(geoStamp.get().getTime());
    			geoEvent.setLongitude(geoStamp.get().getLongitude());
    			geoEvent.setLatitude(geoStamp.get().getLatitude());
    			geoEvent.setOriginalGeoStampId(geoStamp.get().getId());
    	    	geoEvent.setOwnerAccount(account);
    	    	logger.info("creating new geoevent: " + geoEvent.toString());
    	    	return geoEventRepository.save(geoEvent);
    		} else {
    			logger.warn("The account id " + account.getId() + " and geoStamp dont have the same owner. Aborting.");
    		}
    	}
    	return geoEvent;
    	
//    	Account account = accountRepository.findByUserName(principal.getName());
//    	if(geoEvent.getId()==0) {
//    		// Create new
//    		GeoEvent localGeoEvent = new GeoEvent(geoEvent.getEventName(),geoEvent.getDescription(),geoEvent.getLongitude(), geoEvent.getLatitude(),geoEvent.getRadius(),
//    									geoEvent.getTimeSpanBefore(),geoEvent.getTimeSpanAfter(),null);
//    		return geoEventRepository.save(localGeoEvent);
//    	} else {
//    		Optional<GeoEvent> localGeoEvent = geoEventRepository.findById(geoEvent.getId());
//    		if(localGeoEvent.isPresent()) {
//    			try {
//	    			localGeoEvent.get().setEventName(geoEvent.getEventName());
//	    			localGeoEvent.get().setDescription(geoEvent.getDescription());
//	    			localGeoEvent.get().setLongitude(geoEvent.getLongitude());
//	    			localGeoEvent.get().setLatitude(geoEvent.getLatitude());
////	    			localGeoEvent.get().setRadius(geoEvent.getRadius());
////	    			localGeoEvent.get().setTimeSpanBefore(geoEvent.getTimeSpanBefore());
////	    			localGeoEvent.get().setTimeSpanAfter(geoEvent.getTimeSpanAfter());
//    			} catch(NullPointerException e) {
//    				logger.info("Nullpointeception: " + e);
//    			}
//    		} else {
//    			logger.debug("Error: No account in postMyGeoEvents");
//    		}
//    		return geoEventRepository.save(localGeoEvent.get());
//    	}
    }

    @DeleteMapping("/my")
    public void deleteMyGeoEvent(@Valid @RequestBody GeoEvent geoEvent, Principal principal) {
    	Account account = accountRepository.findByUserName(principal.getName());
    	Optional<GeoEvent> geoEvent_ = geoEventRepository.findById(geoEvent.getId());
    	if(geoEvent_.isPresent()) {
    		if(geoEvent_.get().getOwnerAccount().getId() == account.getId()) {
    			geoEventRepository.delete(geoEvent_.get());
    			logger.debug("Removing geoEvent id " + geoEvent_.get().getId());
    		} else {
    			logger.debug("Account is not owner of geoEvent " + geoEvent_.get().getId());
    		}
    	}
    }
    
    @GetMapping("")
    public List<GeoEvent> getAllGeoEvents() {
    	return geoEventRepository.findAll();
    }

    @PostMapping("")
    public GeoEvent createGeoEvent(@Valid @RequestBody GeoEvent geoEvent) {
    	return geoEventRepository.save(geoEvent);
    }

    // Get a Single Note
    @GetMapping("/{id}")
    public GeoEvent getGeoEvent(@PathVariable(value="id") Long geoEventId) {
    		return geoEventRepository.findById(geoEventId)
    		.orElseThrow(() -> new ResourceNotFoundException("GeoEvent", "id", geoEventId));
    }

    @PutMapping("/{id}")
    public GeoEvent updateGeoEvent(@PathVariable(value="id") Long geoEventId, @Valid @RequestBody GeoEvent geoEventDetails) {
    	GeoEvent geoEvent = geoEventRepository.findById(geoEventId)
                .orElseThrow(() -> new ResourceNotFoundException("GeoEvent", "id", geoEventId));

        geoEvent.setEventName(geoEventDetails.getEventName());
        geoEvent.setDescription(geoEventDetails.getDescription());
        geoEvent.setLatitude(geoEventDetails.getLatitude());
        geoEvent.setLongitude(geoEventDetails.getLongitude());
        geoEvent.setRadius(geoEventDetails.getRadius());
        geoEvent.setTime(geoEventDetails.getTime());
        geoEvent.setTimeSpanAfter(geoEventDetails.getTimeSpanAfter());
        geoEvent.setTimeSpanBefore(geoEventDetails.getTimeSpanBefore());
        
        
        GeoEvent updatedGeoEvent = geoEventRepository.save(geoEvent);
        return updatedGeoEvent;
    }
    

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteNote(@PathVariable(value = "id") Long geoEventId) {
        GeoEvent geoEvent = geoEventRepository.findById(geoEventId)
                .orElseThrow(() -> new ResourceNotFoundException("GeoEvent", "id", geoEventId));

        geoEventRepository.delete(geoEvent);

        return ResponseEntity.ok().build();
    }
    
}
