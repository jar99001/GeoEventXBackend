package com.allander.geoevent.controller;

import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/res")
public class ResourceController {
	
	@GetMapping("/csrf)")
	public CsrfToken getCsrf(CsrfToken token) {
		return token;
	}
	
}
