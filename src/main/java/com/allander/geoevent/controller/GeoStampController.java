package com.allander.geoevent.controller;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.allander.geoevent.exception.ResourceNotFoundException;
import com.allander.geoevent.model.Account;
import com.allander.geoevent.model.GeoEvent;
import com.allander.geoevent.model.GeoStamp;
import com.allander.geoevent.repository.AccountRepository;
import com.allander.geoevent.repository.GeoStampRepository;

@RestController
@RequestMapping("/api/geostamps")
public class GeoStampController {

	Logger logger = Logger.getLogger(GeoStampController.class);

    @Autowired
    GeoStampRepository geoStampRepository;
    
    @Autowired
    AccountRepository accountRepository;
    
    @GetMapping("/my")
    public List<GeoStamp> getMyGeoStamps(Principal principal) {
    	Account account = accountRepository.findByUserName(principal.getName());
    	logger.info("Getting geoStamps from user " + principal.getName() + "\n\t More info: " + account);
    	return geoStampRepository.findByAccount(account);
    }

    @PostMapping("/my")
    public GeoStamp createMyGeoStamp(@Valid @RequestBody GeoStamp geoStamp, Principal principal) {
    	Account account = accountRepository.findByUserName(principal.getName());
//    	logger.info("Getting geoStamps from user " + principal.getName() + "\n\t More info: " + account);
//    	GeoStamp geoStamp_ = new GeoStamp(geoStamp.getLongitude(),geoStamp.getLatitude(),account);

    	// Security check after too long strings for database
    	
    	geoStamp.setAccount(account);
    	return geoStampRepository.save(geoStamp);
    }

    @DeleteMapping("/my")
    public void deleteMyGeoStamp(@Valid @RequestBody GeoStamp geoStamp, Principal principal) {
    	Account account = accountRepository.findByUserName(principal.getName());
    	Optional<GeoStamp> geoStamp_ = geoStampRepository.findById(geoStamp.getId());
    	if(geoStamp_.isPresent()) {
    		if(geoStamp_.get().getAccount().getId() == account.getId()) {
    			geoStampRepository.delete(geoStamp_.get());
    			logger.debug("Removing geostamp id " + geoStamp_.get().getId());
    		} else {
    			logger.debug("Account is not owner of geoStamp " + geoStamp_.get().getId());
    		}
    	}
    }

    
    @GetMapping("")
    public List<GeoStamp> getAllGeoStamps() {
    	return geoStampRepository.findAll();
    }

    @PostMapping("")
    public GeoStamp createGeoStamp(@Valid @RequestBody GeoStamp geoStamp) {
    	return geoStampRepository.save(geoStamp);
    }

    // Get a Single Note
    @GetMapping("/{id}")
    public GeoStamp getGeoStamp(@PathVariable(value="id") Long geoStampId) {
    		return geoStampRepository.findById(geoStampId)
    		.orElseThrow(() -> new ResourceNotFoundException("GeoStamp", "id", geoStampId));
    }

    @PutMapping("/{id}")
    public GeoStamp updateGeoStamp(@PathVariable(value="id") Long geoStampId, @Valid @RequestBody GeoStamp geoStampDetails) {
    	GeoStamp geoStamp = geoStampRepository.findById(geoStampId)
                .orElseThrow(() -> new ResourceNotFoundException("GeoStamp", "id", geoStampId));

        geoStamp.setLatitude(geoStampDetails.getLatitude());
        geoStamp.setLongitude(geoStampDetails.getLongitude());
        geoStamp.setTime(geoStampDetails.getTime());
        
        
        GeoStamp updatedGeoStamp = geoStampRepository.save(geoStamp);
        return updatedGeoStamp;
    }
    

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteNote(@PathVariable(value = "id") Long geoStampId) {
        GeoStamp geoStamp = geoStampRepository.findById(geoStampId)
                .orElseThrow(() -> new ResourceNotFoundException("GeoStamp", "id", geoStampId));

        geoStampRepository.delete(geoStamp);

        return ResponseEntity.ok().build();
    }
    
}