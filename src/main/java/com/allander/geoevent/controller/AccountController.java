package com.allander.geoevent.controller;

import java.security.Principal;
import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.allander.geoevent.exception.ResourceNotFoundException;
import com.allander.geoevent.model.Account;
import com.allander.geoevent.repository.AccountRepository;

@RestController
@RequestMapping("/api/accounts")
public class AccountController {

	Logger logger = Logger.getLogger(AccountController.class);
	
    @Autowired
    AccountRepository accountRepository;

    @GetMapping("/my")
    public Account getMyAccount(Principal principal) {
    	Account account = accountRepository.findByUserName(principal.getName());
    	logger.info("Getting information from user: " + principal.getName());
   		return account;
    }
    
    @GetMapping("")
    public List<Account> getAllAccounts() {
    	logger.info("Get all accounts.");
    	return accountRepository.findAll();
    }
    
    @GetMapping("new")
    public void newAccounts() {

    }

    @PostMapping("/new")
    public Account createAccount(@Valid @RequestBody Account account) {
    	logger.info("Saving: " + account.getUserName() + ", " + account.getEmail() + ", " + account.getRole());
    	PasswordEncoder passwordEncoder = new BCryptPasswordEncoder(11);
    	// Receiving role instead of password due to jsonIgnore on account.password
    	return accountRepository.save(new Account(account.getUserName(),account.getEmail(),passwordEncoder.encode(account.getRole()),"USER"));
    }

    // Get a Single Note
    @GetMapping("/{id}")
    public Account getAccount(@PathVariable(value="id") Long accountId) {
    		return accountRepository.findById(accountId)
    		.orElseThrow(() -> new ResourceNotFoundException("Account", "id", accountId));
    }

    @PutMapping("/{id}")
    public Account updateAccount(@PathVariable(value="id") Long accountId, @Valid @RequestBody Account accountDetails) {
    	Account account = accountRepository.findById(accountId)
                .orElseThrow(() -> new ResourceNotFoundException("Account", "id", accountId));

        account.setUserName(accountDetails.getUserName());
        account.setEmail(accountDetails.getEmail());

        Account updatedAccount = accountRepository.save(account);
        return updatedAccount;
    }
    

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteNote(@PathVariable(value = "id") Long accountId) {
        Account account = accountRepository.findById(accountId)
                .orElseThrow(() -> new ResourceNotFoundException("Account", "id", accountId));

        accountRepository.delete(account);

        return ResponseEntity.ok().build();
    }
    
}