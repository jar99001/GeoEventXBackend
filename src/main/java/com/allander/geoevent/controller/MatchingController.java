package com.allander.geoevent.controller;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.allander.geoevent.model.Account;
import com.allander.geoevent.model.GeoEvent;
import com.allander.geoevent.model.GeoStamp;
import com.allander.geoevent.repository.AccountRepository;
import com.allander.geoevent.repository.GeoEventRepository;
import com.allander.geoevent.repository.GeoStampRepository;

@RestController
@RequestMapping("/api/matching")
public class MatchingController {

	Logger logger = Logger.getLogger(MatchingController.class);

    @Autowired
    GeoStampRepository geoStampRepository;

	@Autowired
    GeoEventRepository geoEventRepository;

    @Autowired
    AccountRepository accountRepository;

    @GetMapping("/my")
    public Set<GeoEvent> getMatchingGeoEvents(Principal principal) {
    	Account account = accountRepository.findByUserName(principal.getName());
    	Set<GeoEvent> geoEvents = new HashSet<>();
    	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	
    	logger.info("Getting matching geoevents for user: " + principal.getName() + "\n\t More info: " + account);
    	List<GeoStamp> geoStamps = geoStampRepository.findByAccount(account);
    	if(geoStamps.isEmpty()) return null;

    	logger.info("Date from stamp: " + dateFormat.format(geoStamps.get(0).getTime()));
    	
    	geoStamps.forEach(g->{
        	double lat = (180.0/Math.PI)*(2000.0/6378137.0);
        	double lon = (180.0/Math.PI)*(2000.0/6378137.0)/Math.cos((Math.PI/180.0)*g.getLongitude());

			geoEvents.addAll(geoEventRepository.findAllGeoEvents(
					g.getTime(),
					g.getLongitude(),
					g.getLatitude(),
					lon,
					lat));
    	});    	

    	return geoEvents;
    }

    @PostMapping("/my")
    public GeoEvent ConnectMatchingGeoevent(@Valid @RequestBody GeoEvent geoEvent, Principal principal) {
    	Account account = accountRepository.findByUserName(principal.getName());
    	Optional<GeoEvent> geoEventTemp = geoEventRepository.findById(geoEvent.getId());
    	if(geoEventTemp.isPresent()) {
    		logger.info("IT WAS SUCCESSFUL!!!!");
    		Set<Account> accountList = geoEventTemp.get().getGuestAccounts();
    		accountList.add(account);
    		geoEventTemp.get().setGuestAccounts(accountList);
    		
    		Set<GeoEvent> geoEventGuestList = account.getGuestGeoEvent();
    		geoEventGuestList.add(geoEventTemp.get());
    		account.setGuestGeoEvent(geoEventGuestList);
    		
    		accountRepository.save(account);
        	geoEventRepository.save(geoEventTemp.get());
    	} else {
    		logger.warn("No geoEvent with that id. Is someone hacking?");
    		return null;
    	}

    	return geoEventTemp.get();
    }
}
