package com.allander.geoevent.controller;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.allander.geoevent.model.Account;
import com.allander.geoevent.model.Comment;
import com.allander.geoevent.model.GeoEvent;
import com.allander.geoevent.repository.AccountRepository;
import com.allander.geoevent.repository.CommentRepository;
import com.allander.geoevent.repository.GeoEventRepository;

@RestController
@RequestMapping("/api/comments")
public class CommentController {

	Logger logger = Logger.getLogger(GeoStampController.class);

    @Autowired
    GeoEventRepository geoEventRepository;
    
    @Autowired
    AccountRepository accountRepository;
    
    @Autowired
    CommentRepository controllerRepository;

    @PostMapping("")
    public List<Comment> getGeoEventComments(@Valid @RequestBody GeoEvent geoEvent, Principal principal) {
    	Account account = accountRepository.findByUserName(principal.getName());
    	Optional<GeoEvent> geoEventTemp = geoEventRepository.findById(geoEvent.getId());
    	if(geoEventTemp.isPresent()) {
    		if(geoEventTemp.get().getOwnerAccount().equals(account) || 
    		geoEventTemp.get().getGuestAccounts().contains(account)) {
        		logger.info("Getting comments for geoEvent " + geoEvent.getId() + "\n\t Comments: " + geoEventTemp.get().getComments());
        		return geoEventTemp.get().getComments();
    		} else {
        		logger.info("Account does not own or is a guestAccount to the geoEvent. There is something fishey going on!!!");
    		}
    	}
    	return null;
    }

    @PostMapping("send")
    public List<Comment> sendComment(@Valid @RequestBody Comment comment, Principal principal) {
    	Account account = accountRepository.findByUserName(principal.getName());
    	logger.info("Id for geoEvent to get comments from: " + comment.getGeoEvent());
    	Optional<GeoEvent> geoEventTemp = geoEventRepository.findById(comment.getId());

    	if(geoEventTemp.isPresent()) {
    		if(geoEventTemp.get().getOwnerAccount().equals(account) || 
    		geoEventTemp.get().getGuestAccounts().contains(account)) {
    			List<Comment> allComments = geoEventTemp.get().getComments();
    			allComments.add(new Comment(account.getUserName(),comment.getComment(),geoEventTemp.get()));
    			geoEventTemp.get().setComments(allComments);
    			geoEventRepository.save(geoEventTemp.get());
    			logger.info("Posting comment for geoEvent " + geoEventTemp.get().getId() + "\n\t Comments: " + geoEventTemp.get().getComments());
        		return geoEventTemp.get().getComments();
    		} else {
        		logger.info("Account does not own or is a guestAccount to the geoEvent. There is something fishey going on!!!");
    		}
    	} else {
    		logger.info("The geoEventId does not exists!");
    	}
    	return null;
    }
   
}